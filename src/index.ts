import { promisify } from 'util'
import sqlite from 'sqlite3'

const db = new sqlite.Database('./db.sqlite')
const all = promisify(db.all.bind(db))
const close = promisify(db.close.bind(db))

export const main = async () => {
	await all(`select * from sqlite_master where type='table'`).then(console.log)
	await all(`select * from _migrations`).then(console.log)
	await all(`select * from users`).then(console.log)
}

main().then(
	() => close(),
	x => {
		console.log('# something happens.')
		console.error(x)
		if ('undefined' === typeof process) return
		process.exit(1)
	},
)
