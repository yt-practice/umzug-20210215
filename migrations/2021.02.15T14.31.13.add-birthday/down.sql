-- down migration
begin transaction;
create temporary table t1_backup(id,name,birthday);
insert into t1_backup select id,name,birthday from users;
drop table users;
create table users(id integer primary key, name text);
insert into users select id,name from t1_backup;
drop table t1_backup;
commit;
