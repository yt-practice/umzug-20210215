// @ts-check

const { promises: fs, promises } = require('fs')
const { promisify } = require('util')
const pt = require('path')
const { Umzug } = require('umzug')
const sqlite3 = require('sqlite3')

const db = new sqlite3.Database('./db.sqlite')
const run = promisify(db.run.bind(db))
const all = promisify(db.all.bind(db))
const close = promisify(db.close.bind(db))

const storage = new (class Storage {
	async logMigration(migrationName) {
		const sql = 'insert into _migrations values (?)'
		return run(sql, [migrationName])
	}
	async unlogMigration(migrationName) {
		const sql = 'delete from _migrations where name=?'
		return run(sql, [migrationName])
	}
	async executed() {
		await run('create table if not exists _migrations(name text)')
		const sql = 'select name from _migrations order by name'
		return all(sql).then(r => r.map(n => n.name))
	}
})()

const migrator = new Umzug({
	migrations: {
		glob: 'migrations/*/up.sql',
		resolve: params => {
			const { path: name } = params
			const mig = async (isUp = true) => {
				const fpath = isUp ? name : name.replace(/up\.sql$/, 'down.sql')
				const sql = await fs.readFile(fpath, 'utf-8')
				await new Promise((s, e) => db.exec(sql, x => (x ? e(x) : s())))
			}
			return {
				name: pt.basename(pt.dirname(name)),
				up: async () => mig(true),
				down: async () => mig(false),
			}
		},
	},
	logger: undefined,
	create: {
		template: name => {
			return [
				[name + '/up.sql', '-- up migration\n'],
				[name + '/down.sql', '-- down migration\n'],
			]
		},
		folder: 'migrations',
	},
	storage,
})

exports.umzug = migrator
exports.close = close

if (require.main === module) {
	const argv = process.argv
	const iscreate = argv.findIndex(d => 'create' === d.toLowerCase())
	if (~iscreate)
		argv.splice(iscreate + 1, 0, '--allow-confusing-ordering', '--skip-verify')
	migrator.runAsCLI().then(() => close())
}
